# FlashNAND

Hoping to create a flashrom-like userspace NAND/NOR flash reader/writer. I'm seeing a lot of premade NAND readers on windows (FlashCAT ect.) but none seem to support Linux.

## TODO
 * Support Windows/Linux/OSX
 * Modular support for premade readers or other bitbang chips
 * Maybe use [libFTDI](http://developer.intra2net.com/git/?p=libftdi)
 * Flash chip detection based on [linux-mtd](http://linux-mtd.infradead.org)
 * JTAG?

## Notes
 * NAND flash needs 7 pins for GPIO and 8/16 more for data (x8 or x16 chip) (15 or 23 total)
 * NOR flash needs 4 pins for GPIO and 24 for data (44 total)

## References
 * https://www.j-michel.org/blog/2014/05/27/from-nand-chip-to-files
 * http://spritesmods.com/?art=ftdinand
 * http://www.linux-mtd.infradead.org/nand-data/nanddata.html
 * https://www.flashrom.org/
 * https://www.ece.umd.edu/~blj/CS-590.26/micron-tn2919.pdf
 * https://www.blackhat.com/docs/us-14/materials/us-14-Oh-Reverse-Engineering-Flash-Memory-For-Fun-And-Benefit.pdf
 * https://github.com/ohjeongwook/DumpFlash
 * https://joshuawise.com/projects/ndfslave
 * http://www.uchobby.com/index.php/2007/05/05/read-embedded-flash-chips/

## Hardware
### Supported
|Hardware|Support|URL|Notes|
|--------|-------|---|-----|

### Untested / Potential Support
|Hardware|Support|URL|Notes|
|--------|-------|---|-----|
|FT2232H |TODO   |[Info](http://www.ftdichip.com/Products/ICs/FT2232H.htm)|     |
|FT4232H |TODO   |[Info](http://www.ftdichip.com/Products/ICs/FT4232H.htm)|     |
|FlashCAT USB xPort|TODO|[Store](http://www.embeddedcomputers.net/products/FlashcatUSB_xPort/)|Atmel 90USB646-AU - Vendor software only supports NORx16 or SLC NANDx8 devices. Maximum size 2Gbit NOR or 32Gbit NAND.|
|OpenBench Logic Sniffer v1.04|TODO|[Wiki](http://dangerousprototypes.com/docs/Open_Bench_Logic_Sniffer)|Xilinx Spartan-3E FPGA XC3S250E - 16 unbuffered/direct to FPGA pins and 16 buffered (unusable?) pins.|
|Bus Blaster v4.1a|TODO|[Wiki](http://dangerousprototypes.com/docs/Bus_Blaster)|Xilinx CoolRunner-II CPLD XC2C64A. 24 direct to CPLD pins and 8 pins through 82ohm resistors.

### Unsupported
|Hardware|Support|URL|Notes|
|--------|-------|---|-----|
|FT232R  |Not enough GPIO pins|[Info](http://www.ftdichip.com/Products/ICs/FT232R.htm) |||
